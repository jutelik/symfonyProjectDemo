<?php


namespace App\Tests;


use App\Service\Pizza\PizzaService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\Pizza\BasePizzaInterface;

class PizzaServiceTest extends KernelTestCase
{

    /**
     * @var PizzaService
     */
    private $pizzaService;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->pizzaService = $kernel->getContainer()->get('test.App\Service\Pizza\PizzaService');
    }

    /** @test */
    public function servicePizza() {
        $pizzaObj = $this->pizzaService->createPizza();
        $this->assertInstanceOf("App\Service\Pizza\BasePizzaInterface",$pizzaObj);
        $pizzaObj = $this->pizzaService->addTopping($pizzaObj,"Beef");
        $this->assertEquals("Basic Pizza Adding thin Steak",$pizzaObj->getDescription());
    }

    /** @test */
    public function createCompletePizza()
    {
        $this->assertIsInt($this->pizzaService->createCompletePizza(["Beef","Onion"]));
    }
}