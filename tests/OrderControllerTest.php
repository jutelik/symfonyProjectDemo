<?php


namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderControllerTest extends WebTestCase
{
    /** @test  */
    public function newOrderCreate()
    {
        $client = static::createClient();
        $client->request('POST', '/orders/neworder');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $responseArray = json_decode($client->getResponse()->getContent());
        $this->assertIsInt($responseArray->id);

    }

    /** @test  */
    public function addpizzaToAnOrder()
    {
        //orders can have multiple pizzas && pizzas can have multiple toppings
        $client = static::createClient();

        $client->request('POST', '/orders/addpizza/1',['toppings' => ["Beef","Onion"]]);

        $client->request('POST', '/orders/addpizza/1',['toppings' => ["Beef"]]);
        $jsonObj = json_decode($client->getResponse()->getContent());

        $this->assertGreaterThan(2,$jsonObj->numberOfPizzas);


    }


    /** @test  */
    public function listOrdersAndCounts()
    {
        $url = "/orders";
        $client = static::createClient();
        $client->request('GET', $url);
        //var_dump($client->getResponse()->getContent());
        $this->assertJson($client->getResponse()->getContent());

    }

    /** @test  */
    public function listPizzasForAnOrder()
    {
        $url = "/orders/listpizzas/1";
        $client = static::createClient();
        $client->request('GET', $url);
        $this->assertJson($client->getResponse()->getContent());
    }
}