<?php

namespace App\Service\Pizza;
use App\Service\Pizza\BasePizzaInterface;

class BasePizza implements BasePizzaInterface
{
    const BASE_PRICE = 5.00;
    public function getDescription()
    {
        return "Basic Pizza";
    }

    public function getCost()
    {
        return self::BASE_PRICE;
    }

}