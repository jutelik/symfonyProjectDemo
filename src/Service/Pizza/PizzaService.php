<?php

namespace App\Service\Pizza;

use App\Entity\Pizza as PizzaEntity;
use App\Service\Pizza\BasePizza;
use Doctrine\ORM\EntityManagerInterface;

class PizzaService
{
    const NAMESPACE_TOPPING = __NAMESPACE__."\\";
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return BasePizzaInterface
     */
    public function createPizza():BasePizzaInterface
    {
        return new BasePizza();
    }

    /**
     * @param BasePizzaInterface $pizza
     * @param string $topping
     * @return BasePizzaInterface
     */
    public function addTopping(BasePizzaInterface $pizza,string $topping):BasePizzaInterface
    {
        $toppingClass = self::NAMESPACE_TOPPING.trim($topping)."Topping";
        return new $toppingClass($pizza);
    }


    /**
     * @param BasePizzaInterface $pizzaObj
     * @return int
     */
    public function savePizzaObjToDb(BasePizzaInterface $pizzaObj): int
    {
        $pizzaEntity = new PizzaEntity();
        $pizzaEntity->setName("Pizza test - ".rand(1,22222));
        $pizzaEntity->setDescription($pizzaObj->getDescription());
        $pizzaEntity->setCost($pizzaObj->getCost());

        $this->entityManager->persist($pizzaEntity);
        $this->entityManager->flush();
        return $pizzaEntity->getId();
    }

    public function createCompletePizza($toppings = []): int
    {

        $basePizza = $this->createPizza();
        foreach ($toppings as $topping){
            $basePizza = $this->addTopping($basePizza,$topping);
        }

        return $this->savePizzaObjToDb($basePizza);

    }
}