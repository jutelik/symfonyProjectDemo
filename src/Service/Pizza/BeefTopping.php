<?php


namespace App\Service\Pizza;


class BeefTopping extends BasePizzaDecorator
{
    const COST_BEEF = 2.00;//THis value should come from database...
    const DESC_BEEF = " Adding thin Steak";
    /**
     * @var BasePizzaInterface
     */
    protected $pizza;

    public function __construct(BasePizzaInterface $pizza)
    {
        $this->pizza = $pizza;
    }

    public function getDescription()
    {
        return $this->pizza->getDescription().self::DESC_BEEF;
    }

    public function getCost()
    {
        return $this->pizza->getCost() + self::COST_BEEF;
    }

}