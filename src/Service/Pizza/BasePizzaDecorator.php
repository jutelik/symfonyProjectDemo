<?php


namespace App\Service\Pizza;


abstract class BasePizzaDecorator implements BasePizzaInterface
{

    abstract function getDescription();

    abstract function getCost();
}