<?php


namespace App\Service\Pizza;


class OnionTopping extends BasePizzaDecorator
{
    const COST_ONION = 1.00;//THis value should come from database...
    const DESC_ONION = " Add light Onion";//From setter MeEthods

    /**
     * @var BasePizzaInterface
     */
    protected $pizza;

    public function __construct(BasePizzaInterface $pizza)
    {
        $this->pizza = $pizza;
    }

    public function getDescription()
    {
        return $this->pizza->getDescription().self::DESC_ONION;
    }

    public function getCost()
    {
        return $this->pizza->getCost() + self::COST_ONION;
    }

}