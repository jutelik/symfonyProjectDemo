<?php


namespace App\Service\Pizza;


interface BasePizzaInterface
{
    public function getDescription();//: string;
    public function getCost();//: decimal;

}