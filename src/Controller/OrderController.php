<?php

namespace App\Controller;

use App\Entity\Pizza;
use App\Repository\OrderRepository;
use App\Repository\PizzaRepository;
use App\Service\Pizza\PizzaService;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Entity\Order as OrderEntity;
use App\Entity\Pizza as PizzaEntity;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OrderController
 * @package App\Controller
 */
class OrderController extends AbstractController
{

    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var PizzaService
     */
    private $pizzaService;

    /** @var PizzaRepository  */
    private $pizzaRepository;

    public function __construct(OrderRepository $orderRepository,
                                PizzaRepository $pizzaRepository,
                                PizzaService $pizzaService
    )
    {
        $this->orderRepository = $orderRepository;
        $this->pizzaService = $pizzaService;
        $this->pizzaRepository = $pizzaRepository;
    }

    /**
     * @return JsonResponse
     * @Rest\Route("/orders/neworder", methods={"POST"})
     */
    public function createOrder() {

        $em = $this->getDoctrine()->getManager();
        $order = new OrderEntity();
        $order->setStatus(false);
        $em->persist($order);
        $em->flush();
        $orderId = $order->getId();
        return $this->json([
            'message'=> 'new Order',
            'id'     => $orderId
        ]);

    }

    /**
     * @return JsonResponse
     * @Rest\Route("/orders", methods={"GET"})
     */
    public function index()
    {
        $orders = $this->orderRepository->findAll();

        $ordersArray = [];
        foreach ($orders as $order){
            $count = count($order->getPizzas());
            $ordersArray[$order->getId()] = $count;
        }

        return $this->json($ordersArray);
    }

    /**
     * @Rest\Route("/orders/addpizza/{orderId}", methods={"POST"})
     * @param int $orderId
     * @param Request $request
     * @return JsonResponse
     */
    public function addPizza(int $orderId,Request $request)
    {
        $toppings = $request->get('toppings');
        $em = $this->getDoctrine()->getManager();
        /** @var OrderEntity $order */
        $order = $this->orderRepository->findOneBy(['id' => $orderId]);

        /** @var PizzaEntity $pizza */
        $pizzaID = $this->pizzaService->createCompletePizza($toppings);

        $pizzaAdd = $this->pizzaRepository->findOneBy(['id' => $pizzaID]);
        $order = $order->addPizza($pizzaAdd);
        try {
            $em->persist($order);
        } catch (ORMException $e) {
            echo $e->getMessage();
        }

        $em->flush();

        return $this->json([
          'message' => 'Created Pizza '.$order->getId(),
          'numberOfPizzas' => count($order->getPizzas())
        ]);
    }

    /**
     * @param int $orderId
     * @return JsonResponse
     * @Rest\Route("/orders/listpizzas/{orderId}", methods={"GET"})
     */
    public function listPizzaForAnOrder(int $orderId)
    {
        /** @var Order $order */
        $order = $this->orderRepository->findOneBy(['id' => $orderId]);

        $pizzas = [];
        /** @var PizzaEntity $pizza */
        foreach($order->getPizzas() as  $pizza)
        {
            $pizzas[$pizza->getId()] = $pizza->getDescription()." COST ".$pizza->getCost()."\n";
        }

        return $this->json($pizzas);
    }

    /**
     * @return JsonResponse
     * @Rest\Route("/orders/{id}", methods={"PUT"})
     */
    public function update()
    {
        return $this->json([
            'message' => 'Updating'
        ]);
    }

    /**
     * @return JsonResponse
     * @Rest\Route("/posts/{id}", methods={"DELETE"})
     */
    public function delete()
    {
        return $this->json([
            'message' => 'Deleting'
        ]);
    }
}
