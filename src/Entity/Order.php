<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;



/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Pizza", inversedBy="orders")
     */
    private $pizzas;

    public function __construct() {
        $this->pizzas = new ArrayCollection();
    }

    /**
     * @ORM\Column(type="boolean")
     */
    private $status;



    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection | pizzas[]
     */
    public function getPizzas(): Collection
    {
        return $this->pizzas;
    }

    /**
     * @param ArrayCollection $pizzas
     */
    public function setPizzas(ArrayCollection $pizzas): void
    {
        $this->pizzas = $pizzas;
    }

    public function addPizza(Pizza $pizza): self
    {
        $this->pizzas->add($pizza);
        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }
}
